import { data as nodeList } from './nodes.json';
import { data as edgeList } from './edges.json';

export const nodes = nodeList.map(node => ({
    id: node.metadata.id,
    name: node.data.ip,
    data: node.data,
}));

export const links = edgeList.map(edge => ({
    source: parseInt(edge.start),
    target: parseInt(edge.end),
    type: edge.metadata.type,
    data: edge.data,
}));
